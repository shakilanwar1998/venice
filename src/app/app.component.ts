import { Component} from '@angular/core';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { ConfigService } from 'src/app/services/config/config.service';
import { AlertService } from 'src/app/services/alert/alert.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent{

  userData: any = {};
  baseUrl = this.config.baseUrl;

  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'New Rx',
      url: '/fill-prescription',
      icon: 'color-fill'
    },
    {
      title: 'Refil Rx',
      url: '/refill-prescription',
      icon: 'thermometer'
    },
    {
      title: 'Locations',
      url: '/locations',
      icon: 'pin'
    },
    {
      title: 'Transfer Rx',
      url: '/transfer-prescription',
      icon: 'repeat'
    },
    {
      title: 'Settings',
      url: '/',
      icon: 'settings'
    },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public authService: AuthenticationService,
    private screenOrientation: ScreenOrientation,
    protected localStorage: LocalStorage,
    protected config: ConfigService,
    protected alertService: AlertService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      // if (this.localStorage.getItem("user") != null) {

      this.localStorage.getItem('user').subscribe((data: any) => {
        if (data != null) {
          this.setUser(data['data']['user']);
          this.authService.authenticationState.next(true);
          this.router.navigate(['locations']);
        }
        else {
          this.router.navigate(['login']);
        }

      }, () => {
        this.router.navigate(['login']);
      });
      // }
      // else{
      //   this.router.navigate(['login']);
      // }
    });
  }

  setUser(user){
    this.userData = user;
    sessionStorage.setItem('app_user_id', user.id);
  }

  logout() {
    this.authService.logout();
  }

  loginButtonClick() {
    this.router.navigate(['/login']);
  }
}
