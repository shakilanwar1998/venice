import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config/config.service';
import { LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { AlertService } from 'src/app/services/alert/alert.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient, private config: ConfigService,
    private alertService: AlertService, private loadingController: LoadingController,
    public authService: AuthenticationService, private localStorage: LocalStorage, private transfer: FileTransfer, ) { }

  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };

  async adPatient(formData: NgForm, image, id) {
    let validate = this.patientValidate(formData);
    if (validate == true) {
      const loading = await this.loadingController.create({
        message: 'Submitting Details...',
      });
      await loading.present();

      //let user_id: any;

      // await this.localStorage.getItem('user').subscribe((res: any) => {
      //   user_id = res['data']['user'].id;

        let data = {
          'fname': formData.value.fname,
          'lname': formData.value.lname,
          //'email': formData.value.email,
          'mobile': formData.value.mobile,
          'dob': formData.value.dob,
          //'address': formData.value.address,
          'app_user_id': sessionStorage.getItem('app_user_id'),
          'pharmacy_id': id,
          'postal':formData.value.postal,
          'country':formData.value.country,
          'province':formData.value.province,
          'city':formData.value.city,
          'street':formData.value.street,
          'health_card':formData.value.health_card,
        };

        this.http.post(this.config.yourSiteUrl + "addpatient", data, this.headers)
          .pipe(
            finalize(() => {
              loading.dismiss();
            })
          )
          .subscribe(res => {
            if (res['success']) {
              if (image) {
                this.uploadProfilePic(res['data'], image);
              }
              else {
                this.alertService.alert("Success", "Patient Added successfully", "patients");
              }
            } else {
              this.alertService.presentToast('failed');
            }
          });
      //});
    }
  }


  async uploadProfilePic(res, image) {
    const loading = await this.loadingController.create({
      message: 'Uploading Photo...',
    });
    await loading.present();

    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: "photo",
      fileName: res['data'],
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }

    fileTransfer.upload(image, this.config.yourSiteUrl + "addpatient/image", options).then(data => {
    }, error => {
      loading.dismiss();
      this.alertService.alert("Failed", "Submission failed", "add-patient");
    });

    loading.dismiss();
    this.alertService.alert("Success", "Patient added successfully. <br /> Your prescription ID is: " + res['prescription_id'] + "", "patients");
  }


  async submitPrescription(formData, images) {
    let validate = this.prescriptionValidate(formData);

    if (validate == true) {
      const loading = await this.loadingController.create({
        message: 'Submitting Form...',
      });
      await loading.present();

      let patient = formData.value.patient;
      let patient_type = 1;
      if (formData.value.patient == 'me') {
        patient = 0;
        patient_type = 2;
      }

      let data = {
        'patient_id': patient,
        'instructions': formData.value.instructions,
        'pickup_date': formData.value.pickup_time,
        'app_user_id': sessionStorage.getItem('app_user_id'),
        'patient_type': patient_type
      };
      //console.log(this.authService.checkToken());

      //if (this.authService.checkToken()) {
        this.http.post(this.config.yourSiteUrl + "submit_prescription", data, this.headers)
          .pipe(
            finalize(() => {
              loading.dismiss();
            })
          )
          .subscribe(res => {
            if (res['success']) {
              this.uploadPrescriptionImage(res, images);
            } else {
              this.alertService.presentToast('failed');
            }
          });
      //}
    }
  }

  async uploadPrescriptionImage(res, images) {
    const loading = await this.loadingController.create({
      message: 'Uploading Photo...',
    });
    await loading.present();

    for (let i = 0; i < images.length; i++) {
      const fileTransfer: FileTransferObject = this.transfer.create();
      let options: FileUploadOptions = {
        fileKey: "photo",
        fileName: res['data'],
        chunkedMode: false,
        mimeType: "image/jpeg",
        headers: {}
      }

      fileTransfer.upload(images[i], this.config.yourSiteUrl + "submit_prescription/image", options).then(data => {
      }, error => {
        loading.dismiss();
      });
    }
    loading.dismiss();
    this.alertService.alert("Success", "Pescription submitted successfully. <br /> Your prescription ID is: " + res['prescription_id'] + "", "home");
  }


  async refillPrescription(formData: NgForm, prescription, id) {
    const loading = await this.loadingController.create({
      message: 'Submitting form...',
    });
    await loading.present();

    var data = {
       'patient_id': prescription.patient_id,
       'parent_id': id,
       'instructions': formData.value.instructions,
       'pickup_date': formData.value.pickup_date,
       'app_user_id': prescription.app_user_id,
       'patient_type' : prescription.patient_type
     };
    this.http.post(this.config.yourSiteUrl + "refill_prescription", data, this.headers)
      .pipe(
        finalize(() => {
          loading.dismiss();
        })
      )
      .subscribe(res => {
        if (res['success']) {
          this.alertService.alert("Success", "Pescription submitted successfully. <br /> Your prescription ID is: " + res['prescription_id'] + "", "home");
        }
      });
  }


  async transferPrescription(formData, patient_id) {
    let date = new Date(formData.value.pickup_time);

    const loading = await this.loadingController.create({
      message: 'Submitting Details...',
    });
    await loading.present();

    let delivery = 'No';
    if (formData.value.delivery == true) {
      delivery = 'Yes';
    }

    let patient = patient_id;
    let patient_type = 1;
    if (formData.value.patient == 'me') {
      patient = 0;
      patient_type = 2;
    }

      let data = {
        'pharmacy_name': formData.value.pharmacy_name,
        'phone': formData.value.phone,
        'address': formData.value.address,
        'prescription_number': formData.value.prescription_number,
        'pickup_time': formData.value.pickup_time,
        'delivery': delivery,
        'instructions': formData.value.instructions,
        'patient_id': patient_id,
        'app_user_id': sessionStorage.getItem('app_user_id'),
        'patient_type': patient_type
      };

      this.http.post(this.config.yourSiteUrl + "transfer_prescription", data, this.headers)
        .pipe(
          finalize(() => {
            loading.dismiss();
          })
        )
        .subscribe(res => {
          if (res['success']) {
            this.alertService.alert("Success", "Your Transfer Request Submitted Successfully", "home");
          }
        });

  }

  async submitConsultation(formData: NgForm) {
    let user_id: any;
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();

    await this.localStorage.getItem('user').subscribe((res: any) => {
      user_id = res['data']['user'].id;


      if (formData.value.other != '') {
        var subject = formData.value.other;
      }
      else {
        var subject = formData.value.subject;
      }
      let data = {
        'app_user_id': user_id,
        'subject': subject,
        'comment': formData.value.comment
      };

      this.http.post(this.config.yourSiteUrl + "submitconsultation", data, this.headers)
        .pipe(
          finalize(() => {
            loading.dismiss();
          })
        )
        .subscribe(res => {
          if (res['success']) {
            this.alertService.alert("Success", "Your Consultation Submitted Successfully", "consultation");
          }
        });
    });
  }


  getAllPatients() {
    return this.http.get(this.config.yourSiteUrl + "getallpatients?user_id="+this.config.user_id+"", this.headers);
  }
  getPharmacy(id) {
    return this.http.get(this.config.yourSiteUrl + "getpharmacy?id=" + id + "", this.headers)
  }
  getAllPharmacy() {
    return this.http.get(this.config.yourSiteUrl + "getallpharmacy", this.headers)
  }
  getPatient(id) {
    return this.http.get(this.config.yourSiteUrl + "getpatient?id=" + id + "", this.headers)
  }
  getPatientPrescriptions(id) {
    return this.http.get(this.config.yourSiteUrl + "getpatientprescriptions?id=" + id + "", this.headers)
  }
  getPrescriptionDetails(id) {
    return this.http.get(this.config.yourSiteUrl + "getprescriptiondetails?id=" + id + "", this.headers)
  }

  //Validate

  prescriptionValidate(formData) {
    if (formData.value.patient == '') {
      this.alertService.presentToast('Please select a patient');
      return false;
    }
    else if (formData.value.pickup_date == '') {
      this.alertService.presentToast('Please select a pickup date');
      return false;
    }
    else if (formData.value.pickup_date == '') {
      this.alertService.presentToast('Please select a pickup date');
      return false;
    }
    else if (formData.value.instructions == '') {
      this.alertService.presentToast('Please write some instructions');
      return false;
    }
    else {
      return true;
    }
  }

  patientValidate(formData: NgForm) {
    console.log(formData.value.fname);
    if (formData.value.fname == '') {
      this.alertService.presentToast('Please input first name');
      console.log(formData.value.fname);
      return false;
    }
    else if (formData.value.lname == '') {
      this.alertService.presentToast('Please input last name');
      return false;
    }
    else if (formData.value.mobile == '') {
      this.alertService.presentToast('Please Enter Phone Number');
      return false;
    }
    else if (formData.value.dob == '') {
      this.alertService.presentToast('Please input date of birth');
      return false;
    }
    else if (formData.value.address == '') {
      this.alertService.presentToast('Please Enter Address');
      return false;
    }
    else {
      return true;
    }
  }

  loginValidate(formData: NgForm) {
    if (formData.value.email == '') {
      this.alertService.presentToast('Please Enter Email Address');
      return false;
    }
    else if (formData.value.password == '') {
      this.alertService.presentToast('Please Enter Password');
      return false;
    }

    else {
      return true;
    }
  }


}
