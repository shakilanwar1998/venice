import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert/alert.service';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { ConfigService, TOKEN_KEY} from 'src/app/services/config/config.service';
import { NgForm } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { AppComponent } from 'src/app/app.component';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  authenticationState = new BehaviorSubject(false);
  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
  constructor(
    private http: HttpClient, private loadingController: LoadingController,
    protected localStorage: LocalStorage,private alertService:AlertService,
    private router: Router, private config: ConfigService, public storage: Storage
  ) {
    // this.plt.ready().then(()=>{
    //   this.checkToken();
    // });
  }

  async logout() {
    const loading = await this.loadingController.create({
      message: 'Logging out...',
    });
    await loading.present();
    this.storage.ready().then(() => {
      this.storage.remove(TOKEN_KEY).then(() => {
        this.authenticationState.next(false);
      });
      this.localStorage.clear().subscribe(() => {}, () => {});
      sessionStorage.removeItem('app_user_id');
    });
    loading.dismiss();
    this.router.navigate(['/login']);
  }

  async login(formData: NgForm) {
    let data = formData.value;
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();

    this.http.post(this.config.yourSiteUrl + "login", data, this.headers)
      .pipe(
        finalize(() => {
          loading.dismiss();
        })
      )
      .subscribe(res => {
        if (res['access_token']) {
          this.storage.set(TOKEN_KEY, res['access_token']);
          this.setUser(res['access_token']);
        }
        else {
          this.alertService.alert("Failed", "Incorrect Email or Password", "login");
        }
      });
  }

  async register(formData) {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();

      let data = formData.value;

      this.http.post(this.config.yourSiteUrl + "register", data, this.headers)
        .pipe(
          finalize(() => {
            loading.dismiss();
          })
        )
        .subscribe(res => {
          if (res['access_token']) {
            this.storage.set(TOKEN_KEY, res['access_token']);
            this.setUser(res['access_token']);
            loading.dismiss();
          }
        });

  }

  // async uploadProfilePic(image) {
  //   const loading = await this.loadingController.create({
  //     message: 'Uploading Photo...',
  //   });
  //   await loading.present();
  //
  //   const fileTransfer: FileTransferObject = this.transfer.create();
  //
  //   let options: FileUploadOptions = {
  //     fileKey: "photo",
  //     fileName: 'profile',
  //     chunkedMode: false,
  //     mimeType: "image/jpeg",
  //     headers: {}
  //   }
  //
  //   fileTransfer.upload(image, this.config.yourSiteUrl + "register/image", options).then(data => {
  //     alert(data['image']);
  //   }, error => {
  //     loading.dismiss();
  //   });
  //   loading.dismiss();
  // }


  isAuthenticated() {
    return this.authenticationState.value;
  }

  setUser(token) {
    this.http.get(this.config.yourSiteUrl + "getuser?token=" + token + "", this.headers)
      .subscribe(data => {
        this.localStorage.setItem('user', data).subscribe(() => {}, () => {});
        this.authenticationState.next(true);
        this.router.navigate(['locations']);
      });
  }

  // checkToken(){
  //   this.storage.ready().then(() => {
  //     this.storage.get(TOKEN_KEY).then(token => {
  //       this.http.get(this.config.yourSiteUrl + "getuser?token=" + token + "", this.headers)
  //         .subscribe(res => {
  //           if (res['code'] == 404) {
  //             console.log(res['code']);
  //             sessionStorage.setItem('temp','invalid');
  //           }
  //           else {
  //             sessionStorage.setItem('temp','valid');
  //           }
  //         });
  //     });
  //   });
  //   var validation = sessionStorage.getItem('temp');
  //   if (validation == 'valid') {
  //     return true;
  //   }
  //   else {
  //     return false;
  //   }
  // }
}
