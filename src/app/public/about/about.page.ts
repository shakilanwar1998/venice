import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert/alert.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  constructor(private alertService:AlertService) { }

  ngOnInit() {
  }

  async actionSheet(value){
    this.alertService.tabAction(value);
  }

}
