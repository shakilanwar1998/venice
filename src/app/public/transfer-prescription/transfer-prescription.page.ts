import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { DatetimeService } from 'src/app/services/datetime/datetime.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-transfer-prescription',
  templateUrl: './transfer-prescription.page.html',
  styleUrls: ['./transfer-prescription.page.scss'],
})
export class TransferPrescriptionPage implements OnInit {

  constructor(private router: Router,
    private alertService:AlertService,private loadingController:LoadingController,
    public authService: AuthenticationService,private api:ApiService,
    private dateTimeService: DatetimeService

  ) { }
  result: Observable<any>;
  patients: any = [];
  pharmacy: any = {};
  patient_id: any;
  id = sessionStorage['pharmacy_id'];

  ngOnInit() {
    this.result = this.api.getAllPatients();
    this.result.subscribe(data => {
      this.patients = data;
    });
    console.log(this.pharmacy);
  }

  async actionSheet(value){
    this.alertService.tabAction(value);
  }


  async loadOption() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();

    this.result = this.api.getPharmacy(this.id);
    this.result.subscribe(data => {
      this.pharmacy = data;
      loading.dismiss();
    });
  }

  async submitForm(formData: NgForm) {
    this.api.transferPrescription(formData,this.patient_id);
  }

  timeBtnClick(value) {
    let new_date = this.dateTimeService.getDateTime(value);
    var element = document.getElementById('pickup');
    element.setAttribute('value', new_date);
  }

  patientSelect(value) {
    this.patient_id = value;
  }

  changeColor(id: string) {
    this.dateTimeService.changeColor(id);
  }

}
