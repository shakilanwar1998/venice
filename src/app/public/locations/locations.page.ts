import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { LoadingController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-locations',
  templateUrl: './locations.page.html',
  styleUrls: ['./locations.page.scss'],
})
export class LocationsPage implements OnInit {
    public counter = 0;
  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
  constructor(private router: Router,private api:ApiService,private alertService:AlertService,private platform: Platform,
    private loadingController:LoadingController) {

      this.platform.backButton.subscribe(() => {
        if (this.counter == 0) {
          this.counter++;
          this.alertService.presentToast('Back again to exit');
          setTimeout(() => { this.counter = 0 }, 3000)
        } else {
          // console.log("exitapp");
          navigator['app'].exitApp();
        }
      });
    }

  result: Observable<any>;
  pharmacy: any = [];

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Loading data...',
    });
    await loading.present();
    this.result = this.api.getAllPharmacy();
    this.result.subscribe(data => {
      this.pharmacy = data;
      loading.dismiss();
    });
  }

  cardClick(id){
    sessionStorage.setItem('pharmacy_id',id);
    this.router.navigate(['/home']);
  }

  async actionSheet(value){
    this.alertService.tabAction(value);
  }
}
