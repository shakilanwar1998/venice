import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ConfigService } from 'src/app/services/config/config.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AddPatientPage } from '../add-patient/add-patient.page';
import {LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.page.html',
  styleUrls: ['./patients.page.scss'],
})
export class PatientsPage implements OnInit {
  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
  constructor(public authService: AuthenticationService,private api:ApiService,
  private config: ConfigService,private router: Router,
  public modalController: ModalController,private loadingController: LoadingController,private alertService:AlertService,) { }

  result: Observable<any>;
  patients: any = [];
  baseUrl = this.config.baseUrl;
  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Loading data...',
    });
    await loading.present();

    this.result = this.api.getAllPatients();
    this.result.subscribe(data => {
      this.patients = data;
      loading.dismiss();
    });
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: AddPatientPage
    });
    return await modal.present();
  }


  doRefresh(event) {
    setTimeout(() => {
      this.loadAgain();
      event.target.complete();
    }, 2000);
  }

  loadAgain(){
    this.result = this.api.getAllPatients();
    this.result.subscribe(data => {
      this.patients = data;
    });
  }

  detailsPatient(id){
    this.router.navigate(['/patient-details/'+id+'']);
  }

  async actionSheet(value){
    this.alertService.tabAction(value);
  }

}
