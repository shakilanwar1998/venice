import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefillFormPage } from './refill-form.page';

describe('RefillFormPage', () => {
  let component: RefillFormPage;
  let fixture: ComponentFixture<RefillFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefillFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefillFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
