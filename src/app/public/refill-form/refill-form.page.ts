import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AlertController} from '@ionic/angular';
import { AlertService } from 'src/app/services/alert/alert.service';
import { DatetimeService } from 'src/app/services/datetime/datetime.service';
import { ApiService } from 'src/app/services/api/api.service';
@Component({
  selector: 'app-refill-form',
  templateUrl: './refill-form.page.html',
  styleUrls: ['./refill-form.page.scss'],
})
export class RefillFormPage implements OnInit {
  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
  constructor(private router: Router,private alertService:AlertService,
    private route: ActivatedRoute,private api: ApiService,
    private loadingController: LoadingController, public alertController: AlertController,
    private dateTimeService: DatetimeService
  ) { }

  id:any;
  result: Observable<any>;
  patient: any = [];
  prescription: any = [];

  async ngOnInit() {
    this.route.params.subscribe(params => {
     this.id = params['id'];
    });

    const loading = await this.loadingController.create({
      message: 'Loading data...',
    });
    await loading.present();

    this.result = this.api.getPrescriptionDetails(this.id);
    this.result.subscribe(data => {
      this.prescription = data;
      loading.dismiss();
    });
  }
  dismiss(){
    this.router.navigate(['/refill-prescription']);
  }


  async actionSheet(value){
    this.alertService.tabAction(value);
  }
  async submitForm(formData:NgForm){
    this.api.refillPrescription(formData,this.prescription,this.id);
  }

  changeColor(id: string) {
    this.dateTimeService.changeColor(id);
  }

  timeBtnClick(value) {
    let new_date = this.dateTimeService.getDateTime(value);
    var element = document.getElementById('pickup');
    element.setAttribute('value', new_date);
  }
}
