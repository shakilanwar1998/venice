import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { ApiService } from 'src/app/services/api/api.service';
import { AlertService } from 'src/app/services/alert/alert.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  id = sessionStorage['pharmacy_id'];
  result: Observable<any>;
  pharmacy: any = [];
  public counter = 0;
  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
  constructor(private router: Router, private api: ApiService, private alertService: AlertService,
    private loadingController: LoadingController, public authService: AuthenticationService,
    ) {

  }

  async ngOnInit() {
    if (typeof sessionStorage.pharmacy_id == 'undefined') {
      this.router.navigate(['/locations']);
    }


    const loading = await this.loadingController.create({
      message: 'Loading data...',
    });
    await loading.present();

    this.result = this.api.getPharmacy(this.id);
    this.result.subscribe(data => {
      this.pharmacy = data;
      loading.dismiss();
    });

  }

  async actionSheet(value) {
    this.alertService.tabAction(value);
  }


  menuClick(menu) {
    if (menu == 'fill') {
      this.router.navigate(['/fill-prescription']);
    }
    if (menu == 'refill') {
      this.router.navigate(['/refill-prescription']);
    }
    if (menu == 'location') {
      this.router.navigate(['/locations']);
    }
    if (menu == 'transfer') {
      this.router.navigate(['/transfer-prescription']);
    }
    if (menu == 'call') {
      this.router.navigate(['/call-my-doctor']);
    }
    if (menu == 'deal') {
      this.router.navigate(['/weekly-deals']);
    }
    if (menu == 'patients') {
      this.router.navigate(['/patients']);
    }
    if (menu == 'consultation') {
      this.router.navigate(['/consultation']);
    }
  }

}
