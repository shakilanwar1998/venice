import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { LoadingController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api/api.service';
@Component({
  selector: 'app-refil-prescription',
  templateUrl: './refil-prescription.page.html',
  styleUrls: ['./refil-prescription.page.scss'],
})
export class RefilPrescriptionPage implements OnInit {
  result: Observable<any>;
  patients: any = [];
  prescriptions: any = [];
  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
  constructor(private router: Router,
    private loadingController: LoadingController,private alertService:AlertService,private api:ApiService) { }

  ngOnInit() {
    this.result = this.api.getAllPatients();
    this.result.subscribe(data => {
      this.patients = data;
    });
  }

  async patientSelect(value){
    const loading = await this.loadingController.create({
      message: 'Loading data...',
    });
    await loading.present();
    this.result = this.api.getPatientPrescriptions(value);
    this.result.subscribe(data => {
      this.prescriptions = data;
      loading.dismiss();
    });

  }

  clickPrescription(id){
    this.router.navigate(['/refill-form/'+id+'']);
  }

  async actionSheet(value){
    this.alertService.tabAction(value);
  }

}
