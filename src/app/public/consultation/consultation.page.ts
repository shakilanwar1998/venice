import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert/alert.service';
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.page.html',
  styleUrls: ['./consultation.page.scss'],
})
export class ConsultationPage implements OnInit {

  constructor(private alertService:AlertService,private api:ApiService) { }

  ngOnInit() {
  }

  submitForm(formData:NgForm){
    this.api.submitConsultation(formData);
  }

  async actionSheet(value){
    this.alertService.tabAction(value);
  }

}
