import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { LoadingController } from '@ionic/angular';
import { ConfigService } from 'src/app/services/config/config.service';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.page.html',
  styleUrls: ['./patient-details.page.scss'],
})
export class PatientDetailsPage implements OnInit {
  constructor(private router: Router,private route: ActivatedRoute,private loadingController: LoadingController,
  private config: ConfigService,private alertService:AlertService,
  public authService: AuthenticationService,private api:ApiService) { }
  public headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
  id:any;
  result: Observable<any>;
  patient: any = [];
  prescriptions: any = [];
  baseUrl = this.config.baseUrl;
  async ngOnInit() {
    this.route.params.subscribe(params => {
     this.id = params['id'];
    });

    sessionStorage.setItem('patient',this.id);

    const loading = await this.loadingController.create({
      message: 'Loading data...',
    });
    await loading.present();

    this.result = this.api.getPatient(this.id);
    this.result.subscribe(data => {
      this.patient = data;
      loading.dismiss();
    });

    this.result = this.api.getPatientPrescriptions(this.id);
    this.result.subscribe(data => {
      this.prescriptions = data;
      loading.dismiss();
    });

  }

  clickPrescription(id){
    this.router.navigate(['/prescription-details/'+id+'']);
  }

  async actionSheet(value){
    this.alertService.tabAction(value);
  }

}
