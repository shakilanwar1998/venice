import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { LoadingController } from '@ionic/angular';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { finalize } from 'rxjs/operators';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/services/api/api.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ConfigService } from 'src/app/services/config/config.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  id = sessionStorage['pharmacy_id'];
  image: any;
  public base64Image: string;
  constructor(
    private router: Router, private loadingController: LoadingController, private alertService: AlertService,
    public modalController: ModalController, private localStorage: LocalStorage, private config: ConfigService,
    private authService: AuthenticationService, private camera: Camera,
    private http: HttpClient, private api: ApiService, private transfer: FileTransfer
  ) { }

  regForm(formData: NgForm) {
    this.authService.register(formData);
  }

  ngOnInit() {
  }

  backClick() {
    this.router.navigate(['/login']);
  }
  regloginClick() {
    this.router.navigate(['/login']);
  }

  // selectImage() {
  //   this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
  // }

  // takePicture(sourceType: PictureSourceType) {
  //   const options: CameraOptions = {
  //     quality: 100,
  //     allowEdit: true,
  //     sourceType: sourceType,
  //     destinationType: this.camera.DestinationType.DATA_URL,
  //     correctOrientation: true,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE
  //   }
  //   this.camera.getPicture(options).then((imageData) => {
  //     this.base64Image = "data:image/jpeg;base64," + imageData;
  //     this.image = this.base64Image;
  //     var stringLength = this.image.length - 'data:image/jpeg;base64,'.length;
  //
  //     var sizeInBytes = 4 * Math.ceil((stringLength / 3)) * 0.5624896334383812;
  //     var sizeInKb = sizeInBytes / 1000;
  //
  //     if (sizeInKb > 1024) {
  //       this.image = undefined;
  //       this.alertService.presentToast('Maximum image size is 1 MB');
  //     }
  //
  //   }, (err) => {
  //     console.log(err);
  //   });
  //
  // }

}
